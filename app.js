
var version="1.0";
var simulation = true; 
//var simulation = false; 
var diametro_iniziale = 100.0;//mm
var spessore_carta = 0.1;// mm
var roll2_diametro = 70.0 ; // mm
var mark_size ; //mm
// ------ 
var roll_deg = 0.0 ; // deg
var roll2_deg = 0.0 ; // deg 
var roll2_degT0 = 0.0 ; 
var deg = 0.0; 
// ------
var diametro = diametro_iniziale ; 
var carta = 0.0;
var spins = 0.0;
var pos = 0.0; 
var pi = Math.PI;
var mark_ang = 0 ;
// -------
mark_size = roll2_diametro * pi / 10.0 ; 
console.log(mark_size);
console.log(roll2_diametro * pi);
// -------
var camera;
var scene;
var renderer;
// ---------------------------------------------------------

var uaClient = new UaClient();
uaClient.WebServerHost = '192.168.200.117';
var par_carta = 'ns=2;s=Application.G.carta';
var par_axis1 = 'ns=12;s=Motion.AxisSet.LocalControl.Axis1.ActualValues.ActualPosition';
var par_axis2 = 'ns=12;s=Motion.AxisSet.LocalControl.Axis2.ActualValues.ActualPosition';
var connection_params = {
    ApplicationName: 'MyApplicationName',
    ServerUrlOrDeviceIp: '192.168.200.222',
    TimeOut: 10000,
    StatusCheckTimeOut: 10000,
    StatusCheckInterval: 10000,
    Username: 'boschrexroth',
    Password: 'boschrexroth',
    SecurityMode: 'None',
    AutoReconnect: true
};
// ---------------------------------------------------------
window.onload = init;
window.addEventListener('resize', onResize, false);


var connect=function(){
    uaClient.on('connect', connection_params,

    function (result) {
        console.log("Connected...." + result)
        add_subscription();
    },
    function (result) {
        console.log("Connection Error...." + result)
    });
}

var add_subscription = function() {
    console.log("Add subscription");
    var params = {Items: 
            [{ NodeId: {Id:par_carta}, ClientHandle: 0 },{ NodeId: {Id:par_axis1 }, ClientHandle: 1 }, { NodeId: {Id:par_axis2 }, ClientHandle: 2 } ], 
            PublishingInterval: 10, ClientHandle: 4711 };
    uaClient.on('addSubscription', params ,
    function (result) {
        if (result.ClientHandle == 0) { carta = parseFloat(result.Value);}
        if (result.ClientHandle == 1) { roll_deg = parseFloat(result.Value);}
        if (result.ClientHandle == 2) { roll2_deg = -parseFloat(result.Value);}
    },
    function (result) {
        console.log("Subscription Error " + result); 
    });
}

function get_r( r0, L , s ){return Math.sqrt(L * s / Math.PI + r0 * r0 ) ; }
function get_n( r0, r , s ){return ( r - r0) / s ; }
function get_deg(n)  {return (n*360.0)%360.0;}
function get_rad(n ) {return (n * 2.0 * Math.PI )%(2.0 * Math.PI );}
function process(){
    //---------------------------------------------------------------------
    diametro = 2.0 * get_r(diametro_iniziale/2.0, carta, spessore_carta);
    spins= get_n(diametro_iniziale/2.0, diametro/2.0, spessore_carta);
    //---------------------------------------------------------------------
    roll_deg = get_deg(spins);
    if (simulation){ 
        roll2_deg =get_deg(-carta / (roll2_diametro * pi));
    }
    rate = roll2_diametro/diametro * 100.0; 
 
    document.getElementById("Info-output").innerHTML =  "<table>" + 
    "<tr><td>Spins</td><td>" + show(spins) + 
    "<tr><td>Roll[deg]</td><td>" + show(roll_deg)  + 
    "<tr><td>Roll2[deg]</td><td>" + show(roll2_deg) + 
    "<tr><td>Diameter[mm]</td><td>" + show(diametro) + 
    "<tr><td>Rate[%]</td><td>" + show(rate) + 
    "<tr><td>Paper[Mt]</td><td>" + show(carta/1000.0)  +
    "</table>";
     
}


function init() {
    var d = new Date();
    var n = d.getTime();
    console.log(version);
    console.log(n);
    connect();

    function render() {
        if (simulation){
            if(( spins < controls.spins ) || (controls.rotationSpeed < 0 )) {
                carta += controls.paperSpeed;
            }
        } else {
            //
        }
        process();
   
        rod.scale.x = 50 * controls.scale; 
        rod.scale.z = 50 * controls.scale; 
        rod.scale.y = 100 * controls.scale; 
        rod.rotation.y += (roll_deg/180.0*pi) - rod.rotation.y ;  
        roll.scale.x = diametro/2.0 * controls.scale; 
        roll.scale.z = diametro/2.0 * controls.scale; 
        roll.scale.y = 100 * controls.scale; 

        rod2.scale.x = 50 * controls.scale; 
        rod2.scale.z = 50 * controls.scale; 
        rod2.scale.y = 100 * controls.scale; 
        rod2.position.x = roll2.position.x;
        rod2.rotation.y += (roll2_deg/180.0*pi) - rod2.rotation.y ;  
        roll2.scale.x = roll2_diametro/2.0 * controls.scale; 
        roll2.scale.z = roll2_diametro/2.0 * controls.scale; 
        roll2.scale.y = 100 * controls.scale; 
        roll2.position.x = roll.position.x -  (diametro/2.0 + roll2_diametro/2.0)   * controls.scale;

      

      
        cube.position.y = roll.position.y + diametro /2.0 * controls.scale ;
        cube.position.x = roll.position.x  + 500 * controls.scale ;
        cube.scale.x = 100.0 * controls.scale; 
        cube.scale.z = 50 * controls.scale;

        // --- marks carta
        e = 0;
        from_i = - parseInt(diametro*Math.PI/mark_size/4.0) - 1  ;
        to_i =  parseInt(diametro*Math.PI/mark_size/4.0 * 2.0 )  ;
        ri = get_r(diametro_iniziale/2.0,  carta, spessore_carta);
        ai = get_n(diametro_iniziale/2.0, ri, spessore_carta ) * Math.PI * 2.0 ;
        r = ri; 
        for (e = 0 ; e < 60; e++) {
            i = from_i + e;
            if ( i < to_i ) {
                rn = get_r(diametro_iniziale/2.0,  carta-carta%mark_size - mark_size * i, spessore_carta);
                an = get_n(diametro_iniziale/2.0, rn, spessore_carta ) * Math.PI * 2.0  ;
                a = ai - an - Math.PI ;
                mark[e].position.x = roll.position.x + (Math.cos(a) * r  ) * controls.scale;
                mark[e].position.y = roll.position.y + (Math.sin(a) * r  ) * controls.scale;
                mark[e].scale.y = 50 * controls.scale;
            } else {
                mark[e].position.x =  mark[0].position.x + (i - to_i ) *  mark_size * controls.scale ;
                mark[e].position.y = roll.position.y + diametro/ 2.0 * controls.scale  ;        
                mark[e].scale.y = 50 * controls.scale;
            } 
        } 
     
        // --- marks roll2
        for (i = 0; i < 10; i++) {
            a = i * pi/5.0 + (roll2_deg+controls.offset)/180.0 * pi  ; 
            mark2[i].position.x = roll2.position.x + (Math.cos(a) * roll2_diametro/2.0  ) * controls.scale;
            mark2[i].position.y = roll2.position.y + (Math.sin(a) * roll2_diametro/2.0  ) * controls.scale;
            mark2[i].scale.y = 50 * controls.scale; 
        } 

        stats.update();
        requestAnimationFrame(render);
        camera.lookAt(scene.position);
        renderer.render(scene, camera);
    }
    // -------------- [ Graphics ] -----------------------------
    var stats = initStats();
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(new THREE.Color(0xEEEEEE, 1.0));
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMapEnabled = true;
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enableZoom = true;
    controls.autoRotate = true;
    controls.update();
    // Rod -------------------------------------------------------------------------------
    var geometry = new THREE.CylinderGeometry( 0.8, 0.8, 5.0, 6 );
    var material = new THREE.MeshLambertMaterial({color: 0xc2c2c2});
    var rod = new THREE.Mesh(geometry, material);
    rod.castShadow = true; 
    rod.position = (-14,3,0);
    rod.rotation.x = Math.PI/2.0;
    scene.add(rod);
    // Roll -------------------------------------------------------------------------------
    var geometry = new THREE.CylinderGeometry( 1, 1, 4.9, 32 );
    var material = new THREE.MeshLambertMaterial({color: 0xffffff});
    var roll = new THREE.Mesh(geometry, material);
    roll.castShadow = true;
    roll.position = (-14,3,0);
    roll.rotation.x = Math.PI/2.0;
    scene.add(roll);
    // Rod-Small -------------------------------------------------------------------------------
    var geometry = new THREE.CylinderGeometry( 0.2, 0.2,5.1, 4 );
    var material = new THREE.MeshLambertMaterial({color: 0xc2c2c2});
    var rod2 = new THREE.Mesh(geometry, material);
    rod2.castShadow = true;
    rod2.position = (-14-5, 3,0);
    rod2.rotation.x = Math.PI/2.0;
    scene.add(rod2);
    // Roll-small -------------------------------------------------------------------------------
    var geometry = new THREE.CylinderGeometry( 1, 1, 4.9, 32 );
    var material = new THREE.MeshLambertMaterial({color: 0xffffff});
    var roll2 = new THREE.Mesh(geometry, material);
    roll2.castShadow = true;
    roll2.position =(-14-5,3,0);
    roll2.rotation.x = Math.PI/2.0;
    scene.add(roll2);
    // Marks-small -------------------------------------------------------------------------------
    var geometry = new THREE.CylinderGeometry( 0.1,0.1, 10, 10 );
    var material_red = new THREE.MeshLambertMaterial({color: 0xff0000});
    mark2=[];
    for (i = 0; i < 10; i++) {
        mark2.push( new THREE.Mesh(geometry, material_red) ) ; 
        mark2[i].castShadow = true;
        mark2[i].position.z = 0;
        mark2[i].rotation.x = Math.PI/2;
        scene.add(mark2[i]);
    } 
    // Paper --------------------------------------------------------------------------------
    var geometry = new THREE.BoxGeometry( 10, 0.15, 9.8 );
    var material = new THREE.MeshBasicMaterial( {color: 0xffffff} );
    var cube = new THREE.Mesh( geometry, material );
    cube.castShadow = true;
    cube.position.x = -14 + 5;
    scene.add( cube );
    var geometry = new THREE.CylinderGeometry( 0.1,0.1, 10, 10 );
    var material_green = new THREE.MeshLambertMaterial({color: 0x00ff00});
    mark=[];
    for (i = 0; i < 60; i++) {
        mark.push( new THREE.Mesh(geometry, material_green) ) ; 
        mark[i].castShadow = true;
        mark[i].position.z = 0;
        mark[i].rotation.x = pi/2;
        scene.add(mark[i]);
    } 
    
    // position and point the camera to the center of the scene ---------------------------------
    camera.position.x = -30;
    camera.position.y = 40;
    camera.position.z = 30;
    camera.lookAt(scene.position);
    // add subtle ambient lighting ---------------------------------------------------------------
    var ambientLight = new THREE.AmbientLight(0x0c0c0c);
    scene.add(ambientLight);
    // add spotlight for the shadows -------------------------------------------------------------
    var spotLight = new THREE.SpotLight(0xffffff);
    spotLight.position.set(-40, 60, 100);
    spotLight.castShadow = true;
    scene.add(spotLight);
    // add the output of the renderer to the html element ----------------------------------------
    document.getElementById("WebGL-output").appendChild(renderer.domElement);
    // call the render function ------------------------------------------------------------------
    var controls = new function () {this.paperSpeed = 1; this.scale = 0.05; this.offset = 0.0;this.spins = 50;};
    var gui = new dat.GUI();
    gui.add(controls, 'paperSpeed', -5, 5);
    gui.add(controls, 'offset', -10, 10);
    gui.add(controls, 'spins', 1, 100);
    gui.add(controls, 'scale', 0.01, 0.1);
    
    render(); //-----------------------------------------------------------------------------------

    function initStats() {
        var stats = new Stats();
        stats.setMode(0); // 0: fps, 1: ms
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';
        document.getElementById("Stats-output").appendChild(stats.domElement);
        return stats;
    }
}
function onResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function show(v ) {

    return String(Math.round(v*1000.0)/1000.0);  
}